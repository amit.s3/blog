package route

import (
	"task/src/controller"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func SetupRoutes(router *gin.Engine) {

	api := router.Group("MocktestAPI")
	api.POST("/blog/create", controller.CreateBlogs)
	api.GET("/blog/edit", controller.ListBlogs)
	api.PUT("/blog/update", controller.UpdateBlogs)
	api.DELETE("/blog/delete/", controller.DeleteBlog)
	router.Run(viper.GetString("server.port"))
}
