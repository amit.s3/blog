package main

import (
	"task/config"
	"task/migration"
	"task/route"
	"task/src/repository"
	"task/utils/database"
	logger "task/utils/logging"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func main() {
	config.LoadConfig()
	// viper.SetConfigFile(".env")
	router := gin.Default()
	logger.NewLogger(viper.GetString("logging.level"))
	database.GetInstancemysql()
	migration.Migration()
	repository.MySqlInit()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowCredentials = true
	corsConfig.AllowMethods = []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"}
	corsConfig.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization", "Request-With"}
	router.Use(cors.New(corsConfig))
	route.SetupRoutes(router)
}
