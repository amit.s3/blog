package service

import (
	models "task/src/models"
	"task/src/repository"
	"task/utils/database"
)

type Blogs struct{}

func (c *Blogs) CreateBlogs(insert *models.Blogs) (string, error) {

	if err := repository.Repo.Insert(insert); err != nil {
		return "Unable to insert user", err
	}

	return "User inserted sucessfully", nil
}

func (c *Blogs) ListBlogs(read *models.Blogs) (models.Blogs, error) {
	var blog models.Blogs
	database.DB.Debug().Where("id = ? ", read.Id).Find(&blog)
	return blog, nil
}

func (c *Blogs) UpdateBlogs(update models.Blogs) (string, error) {

	if err := database.DB.Model(&models.Blogs{}).Where("id = ?", update.Id).Updates(map[string]interface{}{
		"id":             update.Id,
		"BlogName":       update.BlogName,
		"BlogType":       update.BlogType,
		"PointOfContact": update.PointOfContact}).Error; err != nil {
		return "Unable to update Blog", err
	}
	return "Blog has been Updated sucessfully", nil
}
func (c *Blogs) DeleteBlogs(Delete int) (string, error) {
	var DeleteBlog models.Blogs
	if err := database.DB.Where("id=?", Delete).Delete(&DeleteBlog).Error; err != nil {
		return "Unable to delete Blog", err
	}
	return "Blog has been Delete sucessfully", nil
}
