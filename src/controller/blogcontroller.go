package controller

import (
	"fmt"
	"net/http"
	"strconv"
	"task/src/models"
	"task/src/service"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

func CreateBlogs(c *gin.Context) {
	CreateBlogs := models.Blogs{}
	if c.ShouldBind(&CreateBlogs) == nil {
		log.Info().Msg("binding successful for params")
	}
	var service = service.Blogs{}
	message, err := service.CreateBlogs(&CreateBlogs)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"apiResponse": "Could not create blog content"})
	} else {
		fmt.Println(message)
		c.JSON(http.StatusOK, gin.H{"apiResponse": "Blog Created"})
	}
}

func ListBlogs(c *gin.Context) {
	var (
		u   models.Blogs
		err error
	)

	var service = service.Blogs{}
	readBlogs, err := service.ListBlogs(&u)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"apiResponse": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"apiResponse": readBlogs})
}

func UpdateBlogs(c *gin.Context) {
	UpdateBlogs := models.Blogs{}
	if c.ShouldBind(&UpdateBlogs) == nil {
		log.Info().Msg("binding successful for params")
	}
	testslackService := service.Blogs{}
	message, err := testslackService.UpdateBlogs(UpdateBlogs)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"apiResponse": err})
	} else {
		fmt.Println(message)
		c.JSON(http.StatusOK, gin.H{"apiResponse": message})
	}
}

func DeleteBlog(c *gin.Context) {
	delete := c.Param("id")
	Delete, _ := strconv.Atoi(delete)
	deleteblog := models.Blogs{}
	if c.ShouldBind(&deleteblog) == nil {
		log.Info().Msg("binding successful for params")
	}
	testslackService := service.Blogs{}
	message, err := testslackService.DeleteBlogs(Delete)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"apiResponse": err})
	} else {
		fmt.Println(message)
		c.JSON(http.StatusOK, gin.H{"apiResponse": "Blog Details Deleted"})
	}
}
