package models

import "task/utils/database"

type Blogs struct {
	Id             int    `gorm:"column:id; PRIMARY_KEY;" json:"id"`
	BlogName       string `gorm:"column:status;type:varchar(255);" json:"blog_name"`
	BlogType       string `" json:"blog_type"`
	PointOfContact string `" json:"poc"`
}

func BlogsMigrate() {
	database.DB.Debug().AutoMigrate(&Blogs{})
}
